# Usar la imagen base de Golang
FROM golang:alpine3.19 AS builder

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copiar los archivos del proyecto al contenedor
COPY . .

# Compilar la aplicación
RUN go build -o main cmd/main.go

# Usar la imagen base de Golang
FROM alpine:3.19

# Establecer el directorio de trabajo dentro del contenedor
WORKDIR /app

# Copiar los archivos del proyecto al contenedor
COPY --from=builder /app/main .

# Copiar los archivos del proyecto al contenedor
COPY config.yaml .

# Exponer el puerto en el que la aplicación escucha
EXPOSE 3000

# Comando para ejecutar la aplicación cuando se inicie el contenedor
CMD ["/app/main"]