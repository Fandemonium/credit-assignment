// Defines the utils package
package utils

// Necessary import lines
import (
	"fmt"
	"strings"

	"github.com/go-playground/validator/v10"
)

// ErrorMessage structure to define the error fields
type ErrorMessage struct {
	Field string `json:"field"`
	Tag   string `json:"tag"`
}

// NewValidator function to create a new validator for domain fields
func NewValidator() *validator.Validate {
	// Create a new validator
	validate := validator.New(validator.WithRequiredStructEnabled())
	// Return the true is valid
	return validate
}

// ValidatorErrors func for show validation errors for each invalid fields.
func ValidatorErrors(err error) string {
	// Define map to add error
	var errors []*ErrorMessage
	// Make error message for each invalid field.
	for _, err := range err.(validator.ValidationErrors) {
		// Create element to add error
		var element ErrorMessage
		// Add key and value from error field
		element.Field = err.Field()
		// Add key and value from error tag
		element.Tag = err.Tag()
		// Append new mmessage to slice message
		errors = append(errors, &element)
	}
	// Define field
	field := strings.ToLower(errors[0].Field)
	// Define tag
	tag := errors[0].Tag
	// Build message error
	message := fmt.Sprintf("Error in the %s field, check the tag: %s, and try again", field, tag)
	// Return the message
	return message
}
