// Defines the utils package
package utils

// Defines structure for build a response
type Response struct {
	Message string `json:"message"`
	Data    any    `json:"data"`
}

// BuildMessage that returns a message of type response
func BuildMessage(message string, data any) *Response {
	// Return the structure response
	return &Response{
		// Add param message
		Message: message,
		// Add param data
		Data: data,
	}
}
