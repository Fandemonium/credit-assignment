// Defines the server package
package server

// Necessary import lines
import (
	"api-rest-yofio/internal/core/ports"
	"api-rest-yofio/pkg/config"
	"log"

	"github.com/gin-gonic/gin"
)

// GinServer structure contains handler and config fields
type Gin struct {
	// Handler configuration field
	Handler ports.CreditAssignerHandler
	// Config configuration field
	Config config.Config
}

// Server interface define contracts for the server
type Server interface {
	// Start function starts the server
	Start()
}

// NewGin function initializes the gin server
func NewGin(config config.Config, handler ports.CreditAssignerHandler) *Gin {
	// Return a structure of Gin
	return &Gin{
		// Config field
		Config: config,
		// Config field
		Handler: handler,
	}
}

// Start function starts the server Gin
func (server *Gin) Start() {

	// Configuring Gin
	gin.SetMode(gin.ReleaseMode)

	// Start the HTTP server
	router := gin.Default()

	// Create group route api
	api := router.Group("/api")

	// Create group route of credit
	creditRoutes := api.Group("/credit")

	// Add a path for the credit assignment
	creditRoutes.POST("/credit-assignment", server.Handler.Assign)
	// Add a path for the credit assignment
	creditRoutes.POST("/statistics", server.Handler.Statistics)

	// Run server
	err := router.Run(":" + server.Config.Server.Port)
	// Error validation
	if err != nil {
		// Show error in console
		log.Fatalln("Error server:", err)
	}
}

// Generate this line for throw error implementations
var _ Server = (*Gin)(nil)
