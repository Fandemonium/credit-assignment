// Defines the config package
package config

// Necessary import lines
import (
	"fmt"

	"github.com/spf13/viper"
)

// Configuration structure contains server and database fields
type Config struct {
	// Server configuration field
	Server ServerConfig `mapstructure:"server"`
	// Database configuration field
	Database DatabaseConfig `mapstructure:"database"`
}

// ServerConfig structure contains port field
type ServerConfig struct {
	// Port configuration field
	Port string `mapstructure:"port"`
}

// DatabaseConfig structure contains database fields
type DatabaseConfig struct {
	Host     string `mapstructure:"host"`
	Port     int    `mapstructure:"port"`
	Username string `mapstructure:"username"`
	Password string `mapstructure:"password"`
	Name     string `mapstructure:"name"`
	SSLMode  string `mapstructure:"ssl_mode"`
}

// LoadConfig function initializes the configuration
func LoadConfig() (Config, error) {
	// Define varibale of type config struct
	var config Config

	// Add config path
	viper.AddConfigPath("./")
	// Set config name
	viper.SetConfigName("config")
	// Set config type
	viper.SetConfigType("yaml")
	// Set config file
	viper.SetConfigFile("./config.yaml")

	// Read configuration from file
	err := viper.ReadInConfig()
	// Error validation
	if err != nil {
		// Return empty config and error
		return config, fmt.Errorf("reading configuration: %s", err)
	}

	// Unbind the configuration to the config structure
	if err := viper.Unmarshal(&config); err != nil {
		// Return empty config and error
		return config, fmt.Errorf("unbinding configuration: %s", err)
	}

	// Return config and nil error
	return config, nil
}
