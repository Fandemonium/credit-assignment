-- Creating the credits table
CREATE TABLE credits (
  id SERIAL PRIMARY KEY,
  credit_type_300 INT NOT NULL,
  credit_type_500 INT NOT NULL,
  credit_type_700 INT NOT NULL,
  investment INT NOT NULL,
  success BOOLEAN NOT NULL
);

-- Add comment in credits table
COMMENT ON TABLE credits IS 'Storage the credits for the YoFio app';