// Defines the handler package
package handler

// Necessary import lines
import (
	"api-rest-yofio/internal/core/domain"
	"api-rest-yofio/internal/core/ports"
	"api-rest-yofio/pkg/utils"
	"net/http"

	"github.com/gin-gonic/gin"
)

// Handler structure contains service field
type Handler struct {
	// Service configuration field
	service ports.CreditAssignerService
}

// NewHandler function initializes a new handler
func NewHandler(service ports.CreditAssignerService) *Handler {
	// Return new handler struct
	return &Handler{
		// Inject service
		service: service,
	}
}

// Assign implements ports.CreditAssignerHandler
func (handler *Handler) Assign(ctx *gin.Context) {
	// Create new investment struct
	investment := &domain.InvestmentRequest{}
	// Create new credits struct to response
	credits := &domain.Credits{}

	// Check, if received JSON data is valid
	if err := ctx.ShouldBindJSON(investment); err != nil {
		// Build the message to response
		message := utils.BuildMessage("The request is malformed", credits)
		// Return the failed response
		ctx.JSON(http.StatusBadRequest, message)
		// Abort execution
		ctx.Abort()
		// Return for exit and not continue
		return
	}

	// Create a new validator
	validate := utils.NewValidator()

	// Validate investment field
	if err := validate.Struct(investment); err != nil {
		// Get details error message
		errorDetail := utils.ValidatorErrors(err)
		// Build the message to response
		message := utils.BuildMessage(errorDetail, credits)
		// Return the failed response
		ctx.JSON(http.StatusBadRequest, message)
		// Abort execution
		ctx.Abort()
		// Return for exit and not continue
		return
	}

	// Invoke service function
	credit300, credit500, credit700, err := handler.service.Assign(investment.Investment)
	// Check, if received JSON data is valid
	if err != nil {
		// Build the message to response
		message := utils.BuildMessage(err.Error(), credits)
		// Return the failed response
		ctx.JSON(http.StatusBadRequest, message)
		// Abort execution
		ctx.Abort()
		// Return for exit and not continue
		return
	}

	// Define response structure
	response := &domain.Credits{
		// Add credit type 300
		CreditType300: credit300,
		// Add credit type 500
		CreditType500: credit500,
		// Add credit type 700
		CreditType700: credit700,
	}
	// Build the message success response
	message := utils.BuildMessage("Credits assigned correctly", response)
	// Return the failed response
	ctx.JSON(http.StatusOK, message)
	// Abort execution
	ctx.Abort()
}

// Statistics implements ports.CreditAssignerHandler.
func (handler *Handler) Statistics(ctx *gin.Context) {
	// Invoke service function
	statistics, err := handler.service.Statistics()
	// Check, if received JSON data is valid
	if err != nil {
		// Build the message to response
		message := utils.BuildMessage(err.Error(), nil)
		// Return the failed response
		ctx.JSON(http.StatusBadRequest, message)
		// Abort execution
		ctx.Abort()
		// Return for exit and not continue
		return
	}
	// Build the message success response
	message := utils.BuildMessage("Statistics", statistics)
	// Return the failed response
	ctx.JSON(http.StatusOK, message)
	// Abort execution
	ctx.Abort()
}

// Generate this line for trow error implementations
var _ ports.CreditAssignerHandler = (*Handler)(nil)
