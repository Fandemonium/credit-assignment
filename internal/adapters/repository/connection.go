// Defines the repository package
package repository

// Necessary import lines
import (
	"api-rest-yofio/pkg/config"
	"context"
	"fmt"

	"github.com/jackc/pgx/v5/pgxpool"
)

// Constant to app name of database
const AppName = "YoFio"

// Connection function to create database connection
func Connection(config config.Config) (*pgxpool.Pool, error) {
	// Get env database min connections
	minConn := 3
	// Get env database max connections
	maxConn := 100
	// MakeURL function to get url connection
	connString := MakeURL(
		// Define user database
		config.Database.Username,
		// Define password database
		config.Database.Password,
		// Define host database
		config.Database.Host,
		// Define port database
		config.Database.Port,
		// Define database
		config.Database.Name,
		// Define SSL mode database
		config.Database.SSLMode,
		// Define min connection database
		minConn,
		// Define max connection database
		maxConn,
	)
	// ParseConfig builds a Config from connString
	connConfig, err := pgxpool.ParseConfig(connString)
	// Error validation
	if err != nil {
		// Return empty connection and error
		return nil, fmt.Errorf("%s %w", "pgxpool.ParseConfig()", err)
	}
	// Save the app name
	connConfig.ConnConfig.RuntimeParams["application_name"] = AppName
	// Create a new pool connection
	pool, err := pgxpool.NewWithConfig(context.Background(), connConfig)
	// Validate error pool connections
	if err != nil {
		// Return error pool connections
		return nil, fmt.Errorf("%s %w", "pgxpool.NewWithConfig()", err)
	}
	// Return pool of connections and nil error
	return pool, nil
}

// MakeURL functions to create url string connections
func MakeURL(user string, pass string, host string, port int, dbName string, sslMode string, minConn int, maxConn int) string {
	// Return string connection
	return fmt.Sprintf("user=%s password=%s host=%s port=%d dbname=%s sslmode=%s pool_min_conns=%d pool_max_conns=%d",
		// Binding params
		user, pass, host, port, dbName, sslMode, minConn, maxConn,
	)
}
