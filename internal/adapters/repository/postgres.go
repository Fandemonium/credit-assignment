// Defines the repository package
package repository

// Necessary import lines
import (
	"api-rest-yofio/internal/core/domain"
	"api-rest-yofio/internal/core/ports"
	"api-rest-yofio/pkg/config"
	"context"
	"log"

	"github.com/jackc/pgx/v5/pgxpool"
)

// PostgresRepository structure contains service database field
type PostgresRepository struct {
	// Database configuration field
	database *pgxpool.Pool
}

// NewPostgresRepository function initializes a new repository
func NewPostgresRepository(config config.Config) *PostgresRepository {
	// Get connection to the database
	database, err := Connection(config)
	// Error validation
	if err != nil {
		// Show error in console
		log.Fatalln("Connection error:", err)
	}
	// Return struct of type PostgresRepository
	return &PostgresRepository{
		// Add database to attribute
		database: database,
	}
}

// StoreCredits implements ports.CreditAssignerRepository
func (postgres *PostgresRepository) StoreCredits(credits *domain.CreditEntity) error {
	// Define query string
	query := `INSERT INTO credits (credit_type_300, credit_type_500, credit_type_700, investment, success) VALUES ($1, $2, $3, $4, $5)`

	// Execute the query
	_, err := postgres.database.Exec(
		// Add background context
		context.Background(),
		// Add query string
		query,
		// Add arguments
		credits.CreditType300, credits.CreditType500, credits.CreditType700, credits.Investment, credits.Success,
	)
	// Validate error insert database
	if err != nil {
		// Return error
		return err
	}
	// Return response successfull
	return nil
}

// GetTotalAssignments implements ports.CreditAssignerRepository.
func (postgres *PostgresRepository) GetTotalAssignments() (int, error) {
	// Define varibale of type int
	var total int
	// Execute an validate error
	err := postgres.database.QueryRow(context.Background(), `SELECT COUNT(*) FROM credits`).Scan(&total)
	// Error validation
	if err != nil {
		// Return zero value and error
		return 0, err
	}
	// Return total and nil error
	return total, nil
}

// GetTotalSuccessfulAssignments implements ports.CreditAssignerRepository.
func (postgres *PostgresRepository) GetTotalSuccessfulAssignments() (int, error) {
	// Define varibale of type int
	var total int
	// Execute an validate error
	err := postgres.database.QueryRow(context.Background(), `SELECT COUNT(*) FROM credits WHERE success = true`).Scan(&total)
	// Error validation
	if err != nil {
		// Return zero value and error
		return 0, err
	}
	// Return total and nil error
	return total, nil
}

// GetTotalUnsuccessfulAssignments implements ports.CreditAssignerRepository.
func (postgres *PostgresRepository) GetTotalUnsuccessfulAssignments() (int, error) {
	// Define varibale of type int
	var total int
	// Execute an validate error
	err := postgres.database.QueryRow(context.Background(), `SELECT COUNT(*) FROM credits WHERE success = false`).Scan(&total)
	// Error validation
	if err != nil {
		// Return zero value and error
		return 0, err
	}
	// Return total and nil error
	return total, nil
}

// GetAverageSuccessfulInvestment implements ports.CreditAssignerRepository.
func (postgres *PostgresRepository) GetAverageSuccessfulInvestment() (float64, error) {
	// Define varibale of type int
	var average float64
	// Execute an validate error
	err := postgres.database.QueryRow(context.Background(), `SELECT AVG(investment) FROM credits WHERE success = true`).Scan(&average)
	// Error validation
	if err != nil {
		// Return zero value and error
		return 0, err
	}
	// Return total and nil error
	return average, nil
}

// GetAverageUnsuccessfulInvestment implements ports.CreditAssignerRepository.
func (postgres *PostgresRepository) GetAverageUnsuccessfulInvestment() (float64, error) {
	// Define varibale of type int
	var average float64
	// Execute an validate error
	err := postgres.database.QueryRow(context.Background(), `SELECT AVG(investment) FROM credits WHERE success = false`).Scan(&average)
	// Error validation
	if err != nil {
		// Return zero value and error
		return 0, err
	}
	// Return total and nil error
	return average, nil
}

// Generate this line for trow error implementations
var _ ports.CreditAssignerRepository = (*PostgresRepository)(nil)
