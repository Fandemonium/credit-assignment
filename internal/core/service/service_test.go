// Defines the service package
package service

// Necessary import lines
import (
	"api-rest-yofio/internal/core/domain"
	"errors"
	"testing"

	"github.com/stretchr/testify/assert"
)

// MockCreditAssignerRepository is a mock implementation of the CreditAssignerRepository interface for testing purposes
type MockCreditAssignerRepository struct{}

// StoreCredits implements ports.CreditAssignerRepository
func (m *MockCreditAssignerRepository) StoreCredits(credits *domain.CreditEntity) error {
	// Simulate successful storage
	return nil
}

// GetTotalAssignments implements ports.CreditAssignerRepository.
func (m *MockCreditAssignerRepository) GetTotalAssignments() (int, error) {
	// Simulate getting total assignments
	return 10, nil
}

// GetTotalSuccessfulAssignments implements ports.CreditAssignerRepository.
func (m *MockCreditAssignerRepository) GetTotalSuccessfulAssignments() (int, error) {
	// Simulate getting total successful assignments
	return 7, nil
}

// GetTotalUnsuccessfulAssignments implements ports.CreditAssignerRepository.
func (m *MockCreditAssignerRepository) GetTotalUnsuccessfulAssignments() (int, error) {
	// Simulate getting total unsuccessful assignments
	return 3, nil
}

// GetAverageSuccessfulInvestment implements ports.CreditAssignerRepository.
func (m *MockCreditAssignerRepository) GetAverageSuccessfulInvestment() (float64, error) {
	// Simulate getting average successful investment
	return 1500.0, nil
}

// GetAverageUnsuccessfulInvestment implements ports.CreditAssignerRepository.
func (m *MockCreditAssignerRepository) GetAverageUnsuccessfulInvestment() (float64, error) {
	// Simulate getting average unsuccessful investment
	return 1000.0, nil
}

// TestAssign functio to test assign
func TestAssign(t *testing.T) {
	// Define mock repo
	mockRepo := &MockCreditAssignerRepository{}
	// Get an instance of service
	svc := NewService(mockRepo)

	// Define struct to test case
	testCases := []struct {
		name       string
		investment int32
		expected   *domain.CreditEntity
		err        error
	}{
		{
			name:       "Valid investment",
			investment: 3000,
			expected: &domain.CreditEntity{
				Investment:    3000,
				CreditType300: 2,
				CreditType500: 2,
				CreditType700: 2,
				Success:       true,
			},
			err: nil,
		},
		{
			name:       "No valid credit assignment found",
			investment: 400,
			expected:   &domain.CreditEntity{},
			err:        errors.New("No valid credit assignment found"),
		},
	}

	// Each test cases
	for _, tc := range testCases {
		// Run the test
		t.Run(tc.name, func(t *testing.T) {
			// Invoke service assign
			credit300, credit500, credit700, err := svc.Assign(tc.investment)
			// Define equal assertions
			assert.Equal(t, tc.err, err, "error does not match")
			// Validate if have error
			if tc.expected != nil {
				// Assert for credit300
				assert.Equal(t, tc.expected.CreditType300, credit300, "credit300 does not match")
				// Assert for credit500
				assert.Equal(t, tc.expected.CreditType500, credit500, "credit500 does not match")
				// Assert for credit700
				assert.Equal(t, tc.expected.CreditType700, credit700, "credit700 does not match")
			}
		})
	}
}

// TestStatistics function to test statistics
func TestStatistics(t *testing.T) {
	// Define mock repo
	mockRepo := &MockCreditAssignerRepository{}
	// Get an instance of service
	svc := NewService(mockRepo)
	// Invoke service statistics
	stats, err := svc.Statistics()
	// Assert error check
	assert.NoError(t, err, "unexpected error")
	// Define structu for the stati
	expectedStats := &domain.Statistics{
		TotalAssignments:              10,
		TotalSuccessfulAssignments:    7,
		TotalUnsuccessfulAssignments:  3,
		AverageSuccessfulInvestment:   1500.0,
		AverageUnsuccessfulInvestment: 1000.0,
	}
	// Check assertions in result
	assert.Equal(t, *expectedStats, *stats, "statistics do not match")
}
