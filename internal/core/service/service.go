// Defines the service package
package service

// Necessary import lines
import (
	"api-rest-yofio/internal/core/domain"
	"api-rest-yofio/internal/core/ports"
	"errors"
	"fmt"
	"math"
	"sync"
)

// Define the types of credits
const (
	Credit300 = int32(300)
	Credit500 = int32(500)
	Credit700 = int32(700)
)

// Service structure contains repository field
type Service struct {
	// Repository configuration field
	repository ports.CreditAssignerRepository
}

// NewService function initializes a new service
func NewService(repository ports.CreditAssignerRepository) *Service {
	// Return new service struct
	return &Service{
		// Inject repository
		repository: repository,
	}
}

// Assign implements ports.CreditAssignerService
func (service *Service) Assign(investment int32) (int32, int32, int32, error) {
	// Define variale of type error
	var err error = nil
	// Defines the variables to store the credit results
	credits := &domain.CreditEntity{
		// Add investment field
		Investment: investment,
	}
	// Defines the minimum difference
	minDifference := int32(math.MaxInt32)

	// Channel to collect results
	resultChan := make(chan *domain.CreditEntity)

	// Iterate the credit of 300, dividing the investment by the credit (300)
	go func() {
		for credit300 := int32(0); credit300 <= investment/Credit300; credit300++ {
			// Iterate the credit of 500, dividing the investment by the credit (500)
			for credit500 := int32(0); credit500 <= investment/Credit500; credit500++ {
				// Iterate the credit of 700, dividing the investment by the credit (700)
				for credit700 := int32(0); credit700 <= investment/Credit700; credit700++ {
					// Validates if the credits are equal to the investment
					if (credit300*Credit300 + credit500*Credit500 + credit700*Credit700) == investment {
						// Get the difference of each credit
						difference := absolute(credit300-credit500) + absolute(credit500-credit700) + absolute(credit700-credit300)
						// Validates if the difference is less than the minimum of the difference
						if difference < minDifference {
							// Add the minimum difference
							minDifference = difference
							// Add the new value for credit 300
							credits.CreditType300 = credit300
							// Add the new value for credit 500
							credits.CreditType500 = credit500
							// Add the new value for credit 700
							credits.CreditType700 = credit700
							// Add the new value for success
							credits.Success = true
						}
					}
				}
			}
		}
		resultChan <- credits
	}()

	// Wait for results from goroutine
	result := <-resultChan

	// Validates if the minimum difference is equal to the maximum integer
	if minDifference == math.MaxInt32 {
		// Add false in value
		result.Success = false
		// Add false in value
		err = errors.New("No valid credit assignment found")
	}

	// Insertamos en la base de datos
	if err := service.repository.StoreCredits(result); err != nil {
		// Returns the best credits and an empty error
		return result.CreditType300, result.CreditType500, result.CreditType700, err
	}

	// Returns the best credits and an empty error
	return result.CreditType300, result.CreditType500, result.CreditType700, err
}

// Statistics implements ports.CreditAssignerService.
func (service *Service) Statistics() (*domain.Statistics, error) {
	// Defines the statistics structure
	statistics := &domain.Statistics{}
	// Define sync of wait group
	var wg sync.WaitGroup
	// Add 5 goroutines
	wg.Add(5)

	// Invoke an anonymous function concurrently
	go func() {
		// Done goroutine end function
		defer wg.Done()
		// Invoke repository to get all assignments
		statistics.TotalAssignments, statistics.Error = service.repository.GetTotalAssignments()
		fmt.Println(statistics.TotalAssignments)
	}()

	// Invoke an anonymous function concurrently
	go func() {
		// Done goroutine end function
		defer wg.Done()
		// Invoke repository to get total success assignments
		statistics.TotalSuccessfulAssignments, statistics.Error = service.repository.GetTotalSuccessfulAssignments()
	}()

	// Invoke an anonymous function concurrently
	go func() {
		// Done goroutine end function
		defer wg.Done()
		// Invoke repository to get total unsuccess assignments
		statistics.TotalUnsuccessfulAssignments, statistics.Error = service.repository.GetTotalUnsuccessfulAssignments()
	}()

	// Invoke an anonymous function concurrently
	go func() {
		// Done goroutine end function
		defer wg.Done()
		// Invoke repository to get average success investment
		statistics.AverageSuccessfulInvestment, statistics.Error = service.repository.GetAverageSuccessfulInvestment()
	}()

	// Invoke an anonymous function concurrently
	go func() {
		// Done goroutine end function
		defer wg.Done()
		// Invoke repository to get average unsuccess investment
		statistics.AverageUnsuccessfulInvestment, statistics.Error = service.repository.GetAverageUnsuccessfulInvestment()
	}()

	// Wait all gorutines
	wg.Wait()
	// Return
	return statistics, statistics.Error
}

// Absolute function returns the number absolutely
func absolute(value int32) int32 {
	// Valid if negative
	if value < 0 {
		// Sign return
		return -value
	}
	// Sign value
	return value
}

// Generate this line for trow error implementations
var _ ports.CreditAssignerService = (*Service)(nil)
