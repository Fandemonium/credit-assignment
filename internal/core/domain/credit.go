// Defines the domain package
package domain

// InvestmentRequest structure that defines the fields of the request
type InvestmentRequest struct {
	// Defines the investment field
	Investment int32 `json:"investment" validate:"required,numeric,gt=0"`
}

// Credit structure that defines the fields of the response
type CreditEntity struct {
	// Defines the investment field
	Investment int32
	// Defines the credit field 300
	CreditType300 int32
	// Defines the credit field 500
	CreditType500 int32
	// Defines the credit field 700
	CreditType700 int32
	// Defines the success field
	Success bool
}

// Credit structure that defines the fields of the response
type Credits struct {
	// Defines the credit field 300
	CreditType300 int32 `json:"credit_type_300"`
	// Defines the credit field 500
	CreditType500 int32 `json:"credit_type_500"`
	// Defines the credit field 700
	CreditType700 int32 `json:"credit_type_700"`
}

// Statistics contains the concurrent results of the queries
type Statistics struct {
	// Defines the total assignments field
	TotalAssignments int `json:"total_assignments"`
	// Defines the total successfull assignments field
	TotalSuccessfulAssignments int `json:"total_successful_assignments"`
	// Defines the total unsuccessfull assignments field
	TotalUnsuccessfulAssignments int `json:"total_unsuccessful_assignments"`
	// Defines the average successfull assignments field
	AverageSuccessfulInvestment float64 `json:"average_successful_investment"`
	// Defines the average unsuccessfull assignments field
	AverageUnsuccessfulInvestment float64 `json:"average_unsuccessful_investment"`
	// Defines the error field
	Error error
}
