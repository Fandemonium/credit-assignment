// Defines the ports package
package ports

// Necessary import lines
import (
	"api-rest-yofio/internal/core/domain"

	"github.com/gin-gonic/gin"
)

// CreditAssignerRepository defines the interface for credit´s repository operations
type CreditAssignerRepository interface {
	// StoreCredits function to store credits in database
	StoreCredits(credits *domain.CreditEntity) error
	// GetTotalAssignments function to get total assignments
	GetTotalAssignments() (int, error)
	// GetTotalSuccessfulAssignments function to get total successfull assignments
	GetTotalSuccessfulAssignments() (int, error)
	// GetTotalUnsuccessfulAssignments function to get total unsuccessfull assignments
	GetTotalUnsuccessfulAssignments() (int, error)
	// GetAverageSuccessfulInvestment function to get average successfull investment
	GetAverageSuccessfulInvestment() (float64, error)
	// GetAverageUnsuccessfulInvestment function to get average unsuccessfull investment
	GetAverageUnsuccessfulInvestment() (float64, error)
}

// CreditAssignerService defines the interface for the credit´s business logic
type CreditAssignerService interface {
	// Assign defines the credit´s business logic
	Assign(investment int32) (int32, int32, int32, error)
	// Assign defines the credit´s business logic
	Statistics() (*domain.Statistics, error)
}

// CreditAssignerHandler defines the interface for the credit´s handler
type CreditAssignerHandler interface {
	// Assign defines the credit´s handler data
	Assign(ctx *gin.Context)
	// Statistics defines the statistics handler
	Statistics(ctx *gin.Context)
}
