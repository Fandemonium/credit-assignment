// Defines the main package
package main

// Necessary import lines
import (
	"api-rest-yofio/internal/adapters/handler"
	"api-rest-yofio/internal/adapters/repository"
	"api-rest-yofio/internal/core/service"
	"api-rest-yofio/pkg/config"
	"api-rest-yofio/pkg/server"
	"log"
)

// Main function is program entry point
func main() {
	// Get the configurations
	config, err := config.LoadConfig()
	// Error validation
	if err != nil {
		// Show error in console
		log.Fatalln("Error loading configurations:", err)
	}
	// Get an instance of repository
	repository := repository.NewPostgresRepository(config)
	// Get an instance of service
	service := service.NewService(repository)
	// Get an instance of handler
	handler := handler.NewHandler(service)
	// Get an instance of new server
	server := server.NewGin(config, handler)
	// Start server
	server.Start()
}
