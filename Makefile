# Define port name
PORT = your_port

# Define host name
HOST = your_host

# Define user name
POSTGRES_USER = your_user

# Define password name
POSTGRES_PASSWORD = your_password

# Define database name
DATABASE = your_database

# Command to run docker container with postgres
postgres:
	docker pull postgres:16-alpine
	docker run --name postgres-dev -p $(PORT):$(PORT) -e POSTGRES_USER=$(POSTGRES_USER) -e POSTGRES_PASSWORD=$(POSTGRES_PASSWORD) -d postgres:16-alpine

# Command to create database
createdb:
	docker exec -it postgres-dev createdb --username=$(POSTGRES_USER) --owner=$(POSTGRES_USER) $(DATABASE)

# Command to drop database
dropdb:
	docker exec -it postgres-dev dropdb --username=$(POSTGRES_USER) $(DATABASE)

# Command to up migrate
migrateup:
	migrate -path migration -database "postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(HOST):$(PORT)/$(DATABASE)?sslmode=disable" -verbose up

# Command to down migrate
migratedown:
	migrate -path migration -database "postgresql://$(POSTGRES_USER):$(POSTGRES_PASSWORD)@$(HOST):$(PORT)/$(DATABASE)?sslmode=disable" -verbose down

# Command to run server
server:
	go run cmd/main.go

# Define name of the homework
.PHONY: postgres createdb dropdb migrateup migratedown server