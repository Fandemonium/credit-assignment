# Credit Assignment - API

This is an API to calculate possible credits based on three types of credits: **credit $300**, **credit $500** and **credit $700**. We obtain the possible amounts of each type of credit based on an investment amount.

## How to configure and execute

To configure and launch our API we must consider the following:

- Go: have [Go](https://go.dev) installed
- Docker: we will need [Docker](https://www.docker.com) to build our database

Considering the previous installations, we proceed with the steps to build our API.

```
$ git clone https://gitlab.com/Fandemonium/credit-assignment.git
$ cd credit-assignment
```

### Database (PostgreSQL)

> **Note:** Before starting to launch the Makefile commands we must configure the **Makefile** file, with the database configurations.

Then we need to build the database, for this we use the **Make** commands, we must execute the following command.

```
$ make postgres
```

Once the postgre container is up, we need to create the database, we must execute the following command.
```
$ make createdb
```

With this we can now perform a migration of our database, we need to run the following command.

```
$ make migrateup
```

> **Note:** To use **migrate** you must have it installed on your device.

### Run server (API)

Before setting up the server we must configure the configuration file (config.yaml) establishing settings such as the server port to the database connection parameters.

To start the server just run the following command.

```
$ make server
```

## Endpoints of the api

You can use the following endpoints

**Credit Assignment**
-   **Route:** `/credit/credit-assignment`
-   **Method HTTP:** POST
-   **Body:** {"investment":  3000}

**Statistics**
-   **Route:** `/credit/statistics`
-   **Method HTTP:** POST# YoFio
